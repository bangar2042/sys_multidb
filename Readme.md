#Systengo Multidb


##Create Virtual env

1. Copy to source project
2. Install dependency (pip install -r requirements)
3. override the settings in production.py And create local.py for local
4. run the migrations
follow the command
python manage.py migrate (will run all the migrations in default databse)
python manage.py migrate products --database=DB1 (will run specific app migrations into given database)
python manage.py migrate products --database=DB2
python manage.py migrate products --database=DB3
python manage.py migrate products --database=DB4
python manage.py migrate products --database=DB5

#To Run the application
either set Environment Variable (you can update it manage.py, i have set it to multidb.settings.local)
for Local 
DJANGO_SETTINGS_MODULE to `multidb.settings.local`

for Production

DJANGO_SETTINGS_MODULE to `multidb.settings.production`



#how to run

1. Create Superuser,
2. login into admin
3. create user (through customized admin add user) (supply username, email, first name, last name, and assigned databases)
4. user will recieved email to set password
5. after set password user will be able to login into system. where he/she can add products, list all, and remove etc.

6. Admin also able to list all the product of all the users. CRUD as well.