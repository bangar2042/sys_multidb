from django.conf.urls import url
from .views import add_product, show_all_products, edit_product, delete_product, admin_show_all_products


urlpatterns = [
    url(r'^add-product/$', add_product, name='add_product'),
    url(r'^edit-product/(?P<product_id>\d+)/(?P<database>\w+)$', edit_product, name='edit_product'),
    url(r'^delete-product/(?P<product_id>\d+)/(?P<database>\w+)$', delete_product, name='delete_product'),
    url(r'^show-all-products/$', show_all_products, name='show_all_products'),
    url(r'^admin-show-all-products/$', admin_show_all_products, name='admin_show_all_products')
]
