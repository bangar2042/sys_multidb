from django import forms
from django.contrib.auth.models import User
from m_db.models import UserDatabases
from django.conf import settings


from .models import Product



class ProductForm(forms.ModelForm):
	user_databases = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)
	class Meta:
		model = Product
		fields = '__all__'

	def __init__(self, user, database=None, *args, **kwargs):
		super(ProductForm, self).__init__(*args, **kwargs)
		self.fields['user'].initial=user.id
		self.fields['user'].widget = forms.HiddenInput()
		try:
			if user.is_superuser:
				assigned_db = [(k, k) for k in settings.DATABASES.keys() if k != 'default']
			else:
				assigned_db = [(k, k) for k in settings.DATABASES.keys() if k in user.userdatabases.databases]
			
			self.fields['user_databases'].choices = assigned_db
			if database:
				self.fields['user_databases'].initial = [database]
				self.fields['user_databases'].widget = forms.MultipleHiddenInput()
		except UserDatabases.DoesNotExist:
			pass
