from django.shortcuts import render, redirect, Http404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.conf import settings

from .models import Product
from .forms import ProductForm
from m_db.models import UserDatabases

# Create your views here.

@login_required
@user_passes_test(lambda u: u.is_superuser)
def admin_show_all_products(request, template_name='products_dashboard.html'):
	user = request.user
	all_products = {}
	databases =  [x for x in settings.DATABASES.keys() if x!='default']
	for each_db in databases:
		all_products[each_db] = Product.objects.using(each_db).all()
	context = {
		'all_products': all_products
	}
	return render(request, template_name, context)

@login_required
def show_all_products(request, template_name='products_dashboard.html'):
	user = request.user
	all_products = {}
	user_databases = user.userdatabases.databases
	for each_db in user_databases:
		all_products[each_db] = Product.objects.using(each_db).filter(user=user.id)
	context = {
		'all_products': all_products
	}
	return render(request, template_name, context)

@login_required
def add_product(request, template_name='add_update_product.html'):
	
	if request.method=='POST':
		add_product_form = ProductForm(request.user,None, request.POST, request.FILES)
		if add_product_form.is_valid():
			user_databases = add_product_form.cleaned_data.pop('user_databases')
			for each_db in user_databases:
				f = add_product_form.save(commit = False)
				f.save(using=each_db)
			if request.user.is_superuser:
				return redirect('products:admin_show_all_products')
			return redirect('products:show_all_products')
	else:
		add_product_form = ProductForm(request.user)

	context = {
		'form':add_product_form
	}

	return render(request, template_name, context)

@login_required
def edit_product(request, product_id, database, template_name='add_update_product.html'):
	try:
		if request.user.is_superuser:
			product = Product.objects.using(database).get(id=product_id)
		else:
			product = Product.objects.using(database).get(id=product_id, user=request.user.id)
	except Product.DoesNotExist:
		raise Http404

	if request.method=='POST':
		update_product_form = ProductForm(request.user, database, request.POST, request.FILES, instance=product)
		if update_product_form.is_valid():
			user_databases = update_product_form.cleaned_data.pop('user_databases')
			for each_db in user_databases:
				f = update_product_form.save(commit = False)
				f.save(using=each_db)
			if request.user.is_superuser:
				return redirect('products:admin_show_all_products')
			return redirect('products:show_all_products')
	else:
		update_product_form = ProductForm(request.user, database, instance=product)

	context = {
		'form':update_product_form,
		'is_update':True
	}
	return render(request, template_name, context)


@login_required
def delete_product(request, product_id, database):
	try:
		if request.user.is_superuser:
			product = Product.objects.using(database).get(id=product_id)
		else:
			product = Product.objects.using(database).get(id=product_id, user=request.user.id)
		product.delete()
		
		if request.user.is_superuser:
			return redirect('products:admin_show_all_products')
		return redirect('products:show_all_products')
	except Product.DoesNotExist:
		raise Http404