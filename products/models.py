from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.functional import cached_property

# Create your models here.

class Product(models.Model):
    name = models.CharField(max_length=128)
    image = models.ImageField(upload_to='products/images', blank=True)
    price = models.IntegerField(default=0)
    user = models.IntegerField(default=None, null=True)

    def __unicode__(self):
        return self.name

    @cached_property
    def username(self):
    	return User.objects.get(id=self.user).username