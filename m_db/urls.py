from django.conf.urls import url
from .views import set_password, user_login, user_logout, index


urlpatterns = [
    url(r'^set-password/(?P<ucode>[\w]+)/$',set_password, name='set_password'),
    url(r'^user-login/$',user_login, name='user_login'),
    url(r'^user-logout/$',user_logout, name='user_logout'),
    url(r'^index/$',index, name='index'),
]
