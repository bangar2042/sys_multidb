from django.contrib import admin
from django.contrib.auth.models import User
from .forms import UserRegistrationForm, UserDatabasesInlineForm
from .models import UserDatabases, ResetPassword

from django.contrib.auth.admin import UserAdmin
from django.contrib import messages

# Register your models here.

admin.site.unregister(User)


class UserDatabasesInline(admin.StackedInline):
	model = UserDatabases
	extra = 1
	max_num = 1
	formset = UserDatabasesInlineForm

@admin.register(User)
class CustomUserAdmin(UserAdmin):
	list_display = (
			'username',
			'email',
			'user_databases'
			)
	inlines = (
			UserDatabasesInline,
			)

	add_form = UserRegistrationForm

	add_fieldsets = (
		(None, {
			'classes': ('wide',),
			'fields': (
					('username','email'),
					('first_name','last_name')
				),
		}),
	)

	add_form_template = None

	def response_add(self, request, obj):
		response = super(UserAdmin, self).response_add(request, obj)
		messages.success(request,"Reset password email has sent to user.")
		return response

	def user_databases(self, obj):
		return obj.userdatabases.databases