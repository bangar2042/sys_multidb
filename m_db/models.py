from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField
from django.conf import settings

# Create your models here.

USER_DATABASES = [(k, k) for k in settings.DATABASES.keys() if k!='default']


class UserDatabases(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	databases = MultiSelectField(choices=USER_DATABASES)

	class Meta:
		verbose_name = 'User Databases'
		verbose_name_plural = 'Users Databases'


class ResetPassword(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	ucode = models.CharField(max_length=20, default=None)
	sent_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(null=True)