from django import forms
from django.forms.models import BaseInlineFormSet
from django.contrib.auth.models import User

class UserRegistrationForm(forms.ModelForm):
	class Meta: 
		modal = User
		fields = ('first_name', 'last_name','email','username')

	def __init__(self, *args, **kwargs):

		#parent constructor
		super(UserRegistrationForm, self).__init__(*args, **kwargs)

		self.fields['email'].required=True

class UserDatabasesInlineForm(BaseInlineFormSet):

	def _construct_form(self, *args, **kwargs):
		"""
		Override the method to change the form attribute empty_permitted
		"""
		form = super(UserDatabasesInlineForm, self)._construct_form(*args, **kwargs)
		form.empty_permitted = False
		return form