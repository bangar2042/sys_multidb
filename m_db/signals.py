from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.shortcuts import reverse
from django.template.loader import get_template

from .models import ResetPassword

from random import choice
import string


@receiver(post_save, sender=User)
def send_password_update_link(sender, instance, created, **kwargs):
	if created:
		ucode = get_ucode()
		ResetPassword.objects.create(ucode=ucode, user=instance)
		subject = 'Confirm Account Registration'
		from_email = settings.EMAIL_HOST_USER
		to = instance.email
		html_content = get_template('user_registration_email.html').render({
			'username':instance.username,
			'set_password_link':settings.SITE_URL+reverse('m_db:set_password',args=(ucode,))
			})
		text_content = html_content.replace('<br>','\n')

		msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
		msg.attach_alternative(html_content, "text/html")
		msg.send()


def get_ucode():
	ucode = ''.join(choice(string.ascii_letters) for _ in range(20))
	if ResetPassword.objects.filter(ucode=ucode):
		return get_ucode()
	return ucode