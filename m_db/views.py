from django.shortcuts import render, reverse,  get_object_or_404, redirect
from django.contrib.auth.forms import SetPasswordForm, AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.conf import settings
from .models import ResetPassword, UserDatabases

# Create your views here.


def set_password(request, ucode, template_name='set_password.html'):
	obj = get_object_or_404(ResetPassword, ucode=ucode)
	user = obj.user
	if request.method=='POST':
		set_password_form = SetPasswordForm(user,request.POST)
		if set_password_form.is_valid():
			set_password_form.save()
			return redirect(reverse('m_db:user_login'))
	else:
		set_password_form = SetPasswordForm(user=user)

	context = {
		'form':set_password_form
	}

	return render(request,
		template_name,
		context
		)

def user_login(request, template_name='user_login.html'):
	if request.user.is_authenticated():
		return redirect(reverse('m_db:index'))
	if request.method == 'POST':
		login_form = AuthenticationForm(data=request.POST)
		if login_form.is_valid():
			login_details = login_form.cleaned_data
			user = authenticate(**login_details)
			if user:
				login(request, user)
				return redirect(reverse('m_db:index'))
			else:
				messages.error(request, "Invalid User")
	else:
		login_form = AuthenticationForm()

	context = {
		'form':login_form
	}
	return render(request,
		template_name,
		context
		)

@login_required
def index(request, template_name='index.html'):
	user = request.user
	try:
		user_databases = user.userdatabases.databases
	except UserDatabases.DoesNotExist:
		pass
	if request.user.is_superuser:
		user_databases = [k for k in settings.DATABASES.keys() if k!='default']
	else:
		user_databases = user.userdatabases.databases
	context = {
		'user_databases':user_databases
	}
	return render(request, template_name, context)

def user_logout(request):
	if request.user.is_authenticated():
		logout(request)
	
	return redirect(reverse('m_db:user_login'))