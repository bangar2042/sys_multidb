from __future__ import unicode_literals

from django.apps import AppConfig


class MDbConfig(AppConfig):
    name = 'm_db'

    def ready(self):
    	import m_db.signals