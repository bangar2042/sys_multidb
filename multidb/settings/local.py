from multidb.settings.base import *

# Override the base.py settings here



# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'multidb',
        'USER':'root',
        'PASSWORD':'Nitt123#',
    },

    'DB1': {
        'NAME': 'pg_database1',
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': 'mdb_user',
        'PASSWORD': 'Nitt123#'
    },

    'DB2': {
        'NAME': 'pg_database2',
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'USER': 'mdb_user',
        'PASSWORD': 'Nitt123#'
    },

    'DB3': {
        'NAME': 'sql_database1',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'root',
        'PASSWORD': 'Nitt123#'
    },

    'DB4': {
        'NAME': 'sql_database2',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'root',
        'PASSWORD': 'Nitt123#'
    },
    'DB5': {
        'NAME': 'sql_database3',
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'root',
        'PASSWORD': 'Nitt123#'
    }
}


STATIC_URL = '/static/'
EMAIL_BACKEND = 'django.core.mail.backends.filebased.EmailBackend'
EMAIL_FILE_PATH = BASE_DIR+'/log-emails/'
EMAIL_HOST_USER = 'admin@example.com'
SITE_URL = 'http://127.0.0.1:8000'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')