Django==1.10
django-extensions==2.1.4
django-multiselectfield==0.1.8
MySQL-python==1.2.5
Pillow==5.3.0
psycopg2==2.7.6.1
six==1.11.0
typing==3.6.6